﻿using System.CommandLine;
using System.Reflection;
using NuGet.Common;
using NuGet.Configuration;
using NuGet.Packaging;
using NuGet.Protocol;
using NuGet.Protocol.Core.Types;

namespace NugetUploader;

public static class Program
{
    public static async Task<int> Main(string[] args)
    {
        var sourceArg = new Argument<string>("server", "Url of the NuGet server");
        var usernameArg = new Argument<string>("username", "Username for the NuGet server");
        var passwordOpt = new Option<string?>(new[] { "-p", "--password" }, "Password or token for the NuGet server");
        var detailedOpt = new Option<bool?>(new[] { "-d", "--detailed" }, "Shows detailed information for each package upload - skipped and published");
        var conflictOpt = new Option<bool?>(new[] { "-t", "--throw-on-conflict" }, "Returns 1 on package conflict");
        var sourceOpt = new Option<string>(new[] { "-s", "--source" }, Directory.GetCurrentDirectory, "Source dictionary");
        var verboseOpt = new Option<bool>(new[] { "-v", "--verbose" }, () => false, "Detailed output");
        var symbolsSuffixOpt = new Option<bool>(new[] { "--symbols-suffix" }, "Upload only the packages with the symbols suffix (foo.bar.1.0.0.symbols.nupkg)");

        var rootCmd = new RootCommand("Publishes all NuGet packages in a folder to the specified NuGet server as long as there is no conflict.");
        rootCmd.AddArgument(sourceArg);
        rootCmd.AddArgument(usernameArg);
        rootCmd.AddOption(passwordOpt);
        rootCmd.AddOption(detailedOpt);
        rootCmd.AddOption(conflictOpt);
        rootCmd.AddOption(sourceOpt);
        rootCmd.AddOption(verboseOpt);
        rootCmd.AddOption(symbolsSuffixOpt);

        rootCmd.SetHandler(Run,
            sourceArg,
            usernameArg,
            passwordOpt,
            detailedOpt,
            sourceOpt,
            verboseOpt,
            conflictOpt,
            symbolsSuffixOpt);

        return await rootCmd.InvokeAsync(args);
    }

    private static void PrintException(Exception? ex)
    {
        var tmp = Console.ForegroundColor;
        Console.ForegroundColor = ConsoleColor.Red;
        Console.WriteLine("An error occured:");

        while (ex != null)
        {
            Console.Write($"[{ex.GetType().FullName}]");
            Console.WriteLine(ex.Message);
            Console.WriteLine(ex.StackTrace);
            Console.WriteLine();

            ex = ex.InnerException;
            if (ex != null)
                Console.WriteLine("<--------- Inner Exception --------->");
        }

        Console.ForegroundColor = tmp;
    }

    private static async Task<int> Run(
        string server,
        string? username,
        string? password,
        bool? showDetailed,
        string sourceDirectory,
        bool verbose,
        bool? throwOnConflict,
        bool symbolsSuffixOpt)
    {
        try
        {
            Console.WriteLine("NuGet Uploader v" + Assembly.GetExecutingAssembly().GetName().Version!.ToString());
            Console.WriteLine();
            if (verbose)
                Console.WriteLine("Source directory: " + sourceDirectory);

            var repository = Repository.Factory.GetCoreV3(server);
            repository.PackageSource.Credentials = new PackageSourceCredential(server, username, password, true, null);

            var resource = await repository.GetResourceAsync<FindPackageByIdResource>();

            var logger = NullLogger.Instance;
            var cancellationToken = CancellationToken.None;
            var cache = new SourceCacheContext();

            var symbolPackages = Directory.GetFiles(sourceDirectory, "*.snupkg", SearchOption.AllDirectories);
            symbolPackages = symbolPackages.Select(x => x[..^7]).ToArray();

            string[] packages;
            if (symbolsSuffixOpt)
                packages = Directory.GetFiles(sourceDirectory, "*.symbols.nupkg", SearchOption.AllDirectories);
            else
                packages = Directory.GetFiles(sourceDirectory, "*.nupkg", SearchOption.AllDirectories);
            packages = packages.Select(x => x[..^6]).ToArray();

            packages = packages.Concat(symbolPackages).ToArray();

            PrintVerbose(verbose, $"Found {packages.Length} packages");
            if (packages.Length == 0)
            {
                Console.WriteLine($"No packages where found of type {(symbolsSuffixOpt ? "*.symbols.nupkg" : "*.nupkg")}");
            }
            else
                foreach (var file in packages)
                {
                    PrintVerbose(verbose, $"Next: {file}");
                    if (!file.StartsWith(sourceDirectory))
                    {
                        PrintVerbose(verbose, "Skipping because file is not in the source directory");
                        continue;
                    }

                    var relativeFile = file[(sourceDirectory.Length + 1)..];
                    if (verbose)
                        Console.WriteLine($"Found Nuget-Package: {relativeFile}.nupkg");

                    using var packageReader = new PackageArchiveReader(file + ".nupkg");
                    var nuspecReader = await packageReader.GetNuspecReaderAsync(cancellationToken);
                    var versions = await resource.GetAllVersionsAsync(nuspecReader.GetId(), cache, logger, cancellationToken);
                    if (versions?.Any(w => w == nuspecReader.GetVersion()) == true)
                    {
                        if (verbose)
                        {
                            if (showDetailed == true)
                                Console.WriteLine(GetDetails(nuspecReader));
                            Console.WriteLine("Skipping upload. Version already exists.");
                        }

                        if (throwOnConflict == true)
                            return 1;
                    }
                    else
                    {
                        if (verbose)
                        {
                            Console.WriteLine(GetDetails(nuspecReader));
                            Console.WriteLine("Upload of Nuget required!");
                        }
                        else
                        {
                            Console.WriteLine($"Uploading {relativeFile}");
                        }

                        var updateResource = await repository.GetResourceAsync<PackageUpdateResource>(cancellationToken);
                        await updateResource.Push(new List<string>() { file + ".nupkg" }, null, 5 * 60, false, _ => null, _ => null, false, true, null, logger);
                    }
                }
            PrintVerbose(verbose, "Finished");
            return 0;
        }
        catch (Exception ex)
        {
            PrintException(ex);
            return ex.HResult;
        }
    }

    private static string GetDetails(NuspecReader nuspecReader)
        => " - Title: " + nuspecReader.GetTitle() + Environment.NewLine +
           " - Version: " + nuspecReader.GetVersion() + Environment.NewLine +
           " - Author: " + nuspecReader.GetAuthors() + Environment.NewLine +
           " - Id: " + nuspecReader.GetId() + Environment.NewLine +
           " - Identity: " + nuspecReader.GetIdentity();

    private static bool PrintVerbose(bool verbose, string text)
    {
        if (verbose)
        {
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine(text);
            Console.ForegroundColor = ConsoleColor.White;
        }
        return verbose;
    }
}
