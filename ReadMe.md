<br />
<div align="center">
  <img src="https://euroident.de/design/logo-euroident-barcodes-dietmannsried.svg" alt="Logo" width="240" height="80">

  <h3 align="center">NuGet Uploader</h3>

  <p align="center">
    Small application for your GitLab Pipeline to keep your NuGet registry clean
  <br />
  <a href="https://gitlab.com/.../-/issues/new">Report Bug</a>
    ·
  <a href="https://gitlab.com/.../-/issues/new">Request Feature</a>
  </p>
</div>

---

### Background

GitLab registry allows multiple packages whith the same name and same version. _(GitLab.com Issue: https://gitlab.com/gitlab-org/gitlab/-/issues/293748)_ \
To work around that critical problem, we created a custom tool which checks if a package with the specific version already exists bevore publishing the package.

---

### Usage

You can either add the application to your git repository or download & extract it while running the deployment stage.\

For the default GitLab registry of your project use the following command call which uses GitLab CI variables.
```
dotnet NugetUploader.dll $CI_API_V4_URL/projects/$CI_PROJECT_ID/packages/nuget/index.json gitlab-ci-token -p $CI_JOB_TOKEN
```

For a normal NuGet server use
```
dotnet NugetUploader.dll https://api.nuget.org/v3/index.json $NUGET_USERNAME --api-key $NUGET_API_KEY
```
For this method you'll need to create two variables in your Project Settings -> CI/CD -> Variables:
- `$NUGET_USERNAME` Your NuGet server username
- `$NUGET_API_KEY` Your NuGet server api key (which can be created at https://www.nuget.org/account/apikeys for the official server)
> You can check `Mask variable` to hide the value of the variables in the job log

---

### Full deployment stage example with runtime download:
```
deploy:
  stage: deploy
  script:
    - mkdir .tmp
    - curl -o ".tmp/nugetuploader.tar.gz" "https://gitlab.com/euroident/nuget-uploader/-/raw/master/.publish/nugetuploader-v1.tar.gz?inline=false"
    - tar -xf ".tmp/nugetuploader.tar.gz" -C ".tmp"
    - dotnet .tmp/NugetUploader.dll $CI_API_V4_URL/projects/$CI_PROJECT_ID/packages/nuget/index.json gitlab-ci-token -p $CI_JOB_TOKEN
  only:
    - master
```

---

### Command

```
Description:
  Publishes all NuGet packages in a folder to the specified NuGet server as long as there is no conflict.

Usage:
  NugetUploader <server> <username> [options]

Arguments:
  <server>    Url of the NuGet server
  <username>  Username for the NuGet server

Options:
  -p, --password <password>  Password or token for the NuGet server
  -d, --detailed             Shows detailed information for each package upload - skipped and published
  -t, --throw-on-conflict    Returns 1 on package conflict
  -s, --source <source>      Source dictionary [default: %execution_directory%]
  --symbols-suffix           Upload only the packages with the symbols suffix (foo.bar.1.0.0.symbols.nupkg)
  --version                  Show version information
  -?, -h, --help             Show help and usage information
```

---

### Licence

See [LICENSE](https://gitlab.com/euroident/nuget-uploader/-/blob/master/LICENSE)

---

### Third Party
| Name | Version | Licence | Link |
| ---- | ------- | ------- | ---- |
| NuGet.Protocol | v6.7.0 | [Apache-2.0] | https://github.com/NuGet/Home |
| System.CommandLine | v2.0.0-beta4.22272.1 | [MIT] | https://github.com/dotnet/command-line-api |
